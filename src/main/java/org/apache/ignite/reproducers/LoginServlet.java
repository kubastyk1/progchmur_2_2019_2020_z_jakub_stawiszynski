package org.apache.ignite.reproducers;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("login");
        String password = request.getParameter("password");
        try {
            User user = new UserDB().getUser(username, password);

            if (user != null) {
                createSession(user, request, response);
            } else {
                handleLoginError(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createSession(User user, HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession();
        session.setAttribute("user", user);
        session.setMaxInactiveInterval(3600);
        Cookie userName = new Cookie("user", user.getUsername());
        userName.setMaxAge(3600);
        response.addCookie(userName);
        response.sendRedirect("mainPage.jsp");
    }

    private void handleLoginError(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("errorMessage", "Wrong login or password");
        getServletContext().getRequestDispatcher("/login.jsp").include(request, response);
    }
}
