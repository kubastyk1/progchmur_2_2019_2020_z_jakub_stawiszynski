package org.apache.ignite.reproducers;

import java.util.Arrays;
import java.util.List;

public class UserDB {

    private final List<User> users = Arrays.asList(
            new User("user1", "pass1"),
            new User("user2", "pass2")
    );

    public User getUser(String username, String password) throws Exception {

        for (User user : users) {
            if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
                return user;
            }
        }

        return null;
    }
}