<%@ page contentType="text/html; utf-8" pageEncoding="utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isELIgnored="false" %>

<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login Page</title>
</head>

<body>

<c:if test="${sessionScope.user != null}">
    <c:redirect url="mainPage.jsp" />
</c:if>

<c:if test="${requestScope.errorMessage != null}">
    <span style="color: red; font-style: italic">
        <c:out value="${requestScope.errorMessage}"/>
    </span>
</c:if>

<h2>Login page</h2>

<form action="${pageContext.request.contextPath}/login" method="post">
    <table>
        <tr>
            <td> Login: <input id="login" type="text" name="login"></td>
        </tr>
        <tr>
            <td> Password: <input id="password" type="password" name="password"></td>
        </tr>
        <tr>
            <td>
                <button type="submit">Login</button>
            </td>
        </tr>
    </table>
</form>

</body>
</html>
